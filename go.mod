module bitbucket.org/aatech_team/golang-dnstap

require (
	bitbucket.org/aatech_team/golang-framestream v0.3.0
	github.com/miekg/dns v1.1.31
	google.golang.org/protobuf v1.23.0
)
